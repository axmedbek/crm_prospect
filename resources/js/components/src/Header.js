import React from 'react';

const Header = () => {
    return (
        <header>
            <div className="logo">
                <a href="#">
                    <img src="img/logo.png" alt="Prospect Cloud Light"/>
                </a>
            </div>
            <div className="header_panel">

                <div className="header_today">

                    <span>Bugün, 04 Oktyabr 2017</span>
                    <span>Çərşənbə</span>

                </div>

                <div className="header_user_tools_group">

                    <ul className="header_user_panel">

                        <li className="messages">

                            <button className="dropdown-toggle" type="button" id="notifications" data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false">
                                <img src="img/icons/envelope.svg" alt="Messages"/>
                                    <span className="badge red">99</span>
                            </button>

                            <div className="dropdown-menu" aria-labelledby="notifications">
                                <div className="header">
                                    - Messages -
                                </div>
                                <div className="body">
                                    <a href="#">
                                        <img src="img/avatar.jpg" alt=""/>
                                            <span className="name">Məmmədbağır Bağırzadəh</span>
                                            <p>Təcili konsert barəsində danışmalıyıq yada yox nese bir soz yazim ki bit
                                                olsun</p>
                                            <time>20 dəq</time>
                                    </a>
                                    <a href="#">
                                        <img src="img/avatar.png" alt=""/>
                                            <span className="name">Məmmədbağır Bağırzadəh</span>
                                            <p>Təcili konsert barəsində danışmalıyıq yada yox nese bir soz yazim ki bit
                                                olsun</p>
                                            <time>20 dəq</time>
                                    </a>
                                    <a href="#">
                                        <img src="img/avatar.png" alt=""/>
                                            <span className="name">Məmmədbağır Bağırzadəh</span>
                                            <p>Təcili konsert barəsində danışmalıyıq yada yox nese bir soz yazim ki bit
                                                olsun</p>
                                            <time>20 dəq</time>
                                    </a>
                                    <a href="#">
                                        <img src="img/avatar.png" alt=""/>
                                            <span className="name">Məmmədbağır Bağırzadəh</span>
                                            <p>Təcili konsert barəsində danışmalıyıq yada yox nese bir soz yazim ki bit
                                                olsun</p>
                                            <time>20 dəq</time>
                                    </a>
                                </div>
                            </div>
                        </li>

                        <li className="notifications">

                            <button className="dropdown-toggle" type="button" id="notifications" data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false">
                                <img src="img/icons/bell.svg" alt="Notifications"/>
                                    <span className="badge red">18</span>
                            </button>

                            <div className="dropdown-menu" aria-labelledby="notifications">
                                <div className="header">
                                    - Notifications -
                                </div>
                                <div className="body">
                                    <a href="#" className="global">
                                        Global notification
                                    </a>
                                    <a href="#" className="cloud">
                                        Cloud noptification
                                    </a>
                                    <a href="#" className="user">
                                        User notification
                                    </a>
                                    <a href="#" className="adds">
                                        Addvertising notification
                                    </a>
                                </div>
                            </div>
                        </li>

                        <li className="holding">

                            <button className="dropdown-toggle" type="button" id="holding" data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false">
                                <span>Gilan Holding</span>
                                <span>
								<img src="img/icons/arrow-down.svg" alt=""/>
							</span>
                            </button>

                            <div className="dropdown-menu" aria-labelledby="holding">
                                <a className="dropdown-item active" href="#">
                                    <img src="img/avatar.jpg"/>
                                        Pasha Insurance
                                </a>
                                <a className="dropdown-item" href="#">
                                    <img src="img/avatar.jpg"/>
                                        CENTRON
                                </a>
                            </div>

                        </li>

                        <li className="user">
                            <button className="dropdown-toggle" type="button" id="user" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                <img src="img/avatar.jpg" alt="Gilan Holding"/>
                            </button>

                            <div className="dropdown-menu" aria-labelledby="user">
                                <a className="dropdown-item" href="dashboard.html">
                                    <i className="fa fa-tachometer"></i> Dashboard
                                    <span className="badge red">5</span>
                                </a>
                                <a className="dropdown-item" href="profile.html">
                                    <i className="fa fa-user"></i> İstifadəçi profili
                                </a>
                                <a className="dropdown-item" href="settings.html">
                                    <i className="fa fa-wrench"></i> Ayarlar
                                    <span className="badge red">5</span>
                                </a>
                                <a className="dropdown-item" href="#">
                                    <i className="fa fa-money"></i> Ödənişlər
                                </a>
                                <a className="dropdown-item" href="#">
                                    <i className="fa fa-sign-out"></i> Sistemdən çıx
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
    );
};

export default Header;