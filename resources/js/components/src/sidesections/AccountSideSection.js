import React from 'react';

class AccountSideSection extends React.Component {
    render() {
        return (
            <section className="sidebar">
                <div className="sidebar_header">
                    <div className="sidebar_header_text">
                        Hesablar
                    </div>

                </div>
                <div className="sidebar_search">
                    <label htmlFor="search">Axtarış</label>
                    <div className="icon-input">
                        <input type="text" className="form-control no-border" id="search" placeholder=""/>
					<span className="icon-input-icon icon-input-icon-right">
						<span>
							<i className="fa fa-search"></i>
						</span>
					</span>
                    </div>
                </div>
                <div className="sidebar_filter" id="filter_menu">
                    <div className="filter_container">
                        <div className="filter_self">
                            <div className="filter_block open">
                                <div className="header">
                                    Qoşulma Müddəti
                                </div>
                                <div className="body">
                                    <ul>
                                        <li>
                                            <div className="checkbox checkbox-success">
                                                <input id="checkbox001" type="checkbox"/>
                                                    <label htmlFor="checkbox001">
                                                        Hamısı
                                                    </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="checkbox checkbox-success">
                                                <input id="checkbox002" type="checkbox"/>
                                                    <label htmlFor="checkbox002">
                                                        14
                                                    </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="checkbox checkbox-success">
                                                <input id="checkbox003" type="checkbox"/>
                                                    <label htmlFor="checkbox003">
                                                        > 14
                                                    </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="checkbox checkbox-success">
                                                <input id="checkbox004" type="checkbox"/>
                                                    <label htmlFor="checkbox004">
                                                        Istənilən gün
                                                    </label>
                                            </div>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                            <div className="filter_block">
                                <div className="header">
                                    Son Aktivlik
                                </div>
                                <div className="body">
                                    <ul>
                                        <li>
                                            <div className="checkbox checkbox-success">
                                                <input id="checkbox01" type="checkbox"/>
                                                    <label htmlFor="checkbox01">
                                                        Hamisi
                                                    </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="checkbox checkbox-success">
                                                <input id="checkbox02" type="checkbox"/>
                                                    <label htmlFor="checkbox02">
                                                        1 Həftə
                                                    </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="checkbox checkbox-success">
                                                <input id="checkbox03" type="checkbox"/>
                                                    <label htmlFor="checkbox03">
                                                        1 ay
                                                    </label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default AccountSideSection;