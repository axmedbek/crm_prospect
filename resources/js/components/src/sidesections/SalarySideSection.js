import React from 'react';

class SalarySideSection extends React.Component {
    render() {
        return (
            <section className="sidebar">
                <div className="sidebar_header">
                    <div className="sidebar_header_text">
                        Ödənişlər
                    </div>

                </div>
                <div className="sidebar_search">
                    <label htmlFor="search">Axtarış</label>
                    <div className="icon-input">
                        <input type="text" className="form-control no-border" id="search" placeholder=""/>
					<span className="icon-input-icon icon-input-icon-right">
						<span>
							<i className="fa fa-search"></i>
						</span>
					</span>
                    </div>
                </div>
                <div className="sidebar_filter" id="filter_menu">
                    <div className="filter_container">
                        <div className="filter_self">
                            <div className="filter_block open">
                                <div className="header">
                                    Tarix
                                </div>
                                <div className="body">
                                    <ul>
                                        <li>
                                            <div className="checkbox checkbox-success">
                                                <input id="checkbox001" type="checkbox"/>
                                                    <label htmlFor="checkbox001">
                                                        Bu gün
                                                    </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="checkbox checkbox-success">
                                                <input id="checkbox002" type="checkbox"/>
                                                    <label htmlFor="checkbox002">
                                                        Bu həftə
                                                    </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="checkbox checkbox-success">
                                                <input id="checkbox003" type="checkbox"/>
                                                    <label htmlFor="checkbox003">
                                                        Bu ay
                                                    </label>
                                            </div>
                                        </li>
                                        <li className="fixed">
                                            <div>
                                                <label htmlFor="from">From</label>
                                                <input type="text" id="from" className="form-control no-border"/>
                                            </div>
                                            <div>
                                                <label htmlFor="to">To</label>
                                                <input type="text" id="to" className="form-control no-border"/>
                                            </div>

                                        </li>

                                    </ul>
                                </div>
                            </div>
                            <div className="filter_block">
                                <div className="header">
                                    Hardan Keçib
                                </div>
                                <div className="body">
                                    <ul>
                                        <li>
                                            <div className="checkbox checkbox-success">
                                                <input id="checkbox01" type="checkbox"/>
                                                    <label htmlFor="checkbox01">
                                                        Hamisi
                                                    </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="checkbox checkbox-success">
                                                <input id="checkbox02" type="checkbox"/>
                                                    <label htmlFor="checkbox02">
                                                        Free
                                                    </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="checkbox checkbox-success">
                                                <input id="checkbox03" type="checkbox"/>
                                                    <label htmlFor="checkbox03">
                                                        Office
                                                    </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="checkbox checkbox-success">
                                                <input id="checkbox04" type="checkbox"/>
                                                    <label htmlFor="checkbox04">
                                                        Office +
                                                    </label>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="checkbox checkbox-success">
                                                <input id="checkbox05" type="checkbox"/>
                                                    <label htmlFor="checkbox05">
                                                        Company
                                                    </label>
                                            </div>
                                        </li>

                                    </ul>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default SalarySideSection;