import React from 'react';
import AccountSideSection from '../sidesections/AccountSideSection';

class Account extends React.Component {
    render() {
        return (
            <div>
                <AccountSideSection/>
                <section className="content">
                    <div className="container-fluid">
                        <div className="content_header_btn_group">
                            <div className="row">
                                <div className="col">
                                    <div className="tile_btn btn-white active_btn">
                                        <a href="accounts.html">
                                            <span>Free</span>
                                            <span className="green-text">100</span>
                                        </a>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="tile_btn btn-white ">
                                        <a href="office.html">
                                            <span>Office</span>
                                            <span className="blue-text">25</span>
                                        </a>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="tile_btn btn-white">
                                        <a href="office-plus.html">
                                            <span>Office +</span>
                                            <span className="yellow-text">30</span>
                                        </a>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="tile_btn btn-white">
                                        <a href="company.html">
                                            <span>Company</span>
                                            <span className="red-text">25</span>
                                        </a>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="tile_btn btn-white">
                                        <a href="cancel.html">
                                            <span>Imtina</span>
                                            <span className="orange-text">30</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="content_tools_line" style={{display: ''}}>
                            <div className="row">
                                <div className="col-md-6 not-tooltips">

                                    <div className="row">
                                        <div className="col">

                                            <div className="content_page_links">
                                                <a href="#">
                                                    <svg data-toggle='tooltip' id='Capa_1'
                                                         xmlns='http://www.w3.org/2000/svg'
                                                         viewBox='0 0 490 490'>
                                                        <path
                                                            d='M393.872,454.517c-2.304,0-4.583-0.804-6.412-2.354l-99.315-84.194H68.115C30.556,367.968,0,337.242,0,299.474V103.977 c0-37.768,30.556-68.494,68.115-68.494h353.77c37.559,0,68.115,30.727,68.115,68.494v195.497 c0,37.768-30.556,68.494-68.115,68.494h-18.071v76.549c0,3.891-2.243,7.428-5.752,9.067 C396.723,454.21,395.293,454.517,393.872,454.517z M68.115,55.483c-26.592,0-48.226,21.754-48.226,48.494v195.497 c0,26.739,21.634,48.494,48.226,48.494h223.662c2.346,0,4.616,0.834,6.411,2.354l85.737,72.685v-65.039c0-5.523,4.453-10,9.945-10 h28.015c26.592,0,48.226-21.755,48.226-48.494V103.977c0-26.74-21.634-48.494-48.226-48.494H68.115z'
                                                        />
                                                        <path
                                                            d='M405.168,147.469H84.832c-5.492,0-9.944-4.478-9.944-10c0-5.523,4.452-10,9.944-10h320.335c5.492,0,9.944,4.477,9.944,10 C415.111,142.991,410.66,147.469,405.168,147.469z'
                                                        />
                                                        <path
                                                            d='M405.168,211.503H84.832c-5.492,0-9.944-4.478-9.944-10c0-5.523,4.452-10,9.944-10h320.335c5.492,0,9.944,4.477,9.944,10 C415.111,207.025,410.66,211.503,405.168,211.503z'
                                                        />
                                                        <path
                                                            d='M405.168,275.538H84.832c-5.492,0-9.944-4.478-9.944-10c0-5.523,4.452-10,9.944-10h320.335c5.492,0,9.944,4.476,9.944,10 C415.111,271.06,410.66,275.538,405.168,275.538z'
                                                        />
                                                    </svg>
                                                </a>
                                            </div>
                                        </div>
                                        <div className="col">

                                            <div className="content_page_links">
                                                <a href="#">
                                                    <svg data-toggle='tooltip' id='Layer_1'
                                                         xmlns='http://www.w3.org/2000/svg'
                                                         viewBox='0 0 512 512'>
                                                        <path
                                                            d='M469.333,64H42.667C19.135,64,0,83.135,0,106.667v298.667C0,428.865,19.135,448,42.667,448h426.667 C492.865,448,512,428.865,512,405.333V106.667C512,83.135,492.865,64,469.333,64z M42.667,85.333h426.667 c1.168,0,2.177,0.486,3.294,0.667L267.333,252.677c-6.938,4.385-16.854,3.677-21.906,0.563L39.365,86.003 C40.484,85.82,41.496,85.333,42.667,85.333z M490.667,405.333c0,11.76-9.573,21.333-21.333,21.333H42.667 c-11.76,0-21.333-9.573-21.333-21.333V106.667c0-2.385,0.637-4.585,1.362-6.728l210.221,170.54 c6.927,4.479,14.917,6.854,23.083,6.854c7.875,0,15.563-2.198,22.313-6.375c0.625-0.323,1.219-0.708,1.76-1.156l209.23-169.868 c0.727,2.145,1.363,4.346,1.363,6.733V405.333z'
                                                        />
                                                    </svg>
                                                </a>
                                            </div>
                                        </div>

                                        <div className="col">

                                            <div className="content_page_links">
                                                <a href="#">
                                                    <svg data-toggle='tooltip' id='Layer_1'
                                                         xmlns='http://www.w3.org/2000/svg'
                                                         viewBox='0 0 512 512'>
                                                        <path
                                                            d='M503.438,275.948l-54.063-54.052c-5.521-5.521-12.854-8.563-20.646-8.563h-23.396V106.667 c0-23.531-19.146-42.667-42.667-42.667h-320C19.146,64,0,83.135,0,106.667V320c0,23.531,19.146,42.667,42.667,42.667h234.667 v2.052c0,7.802,3.042,15.146,8.563,20.667l54.063,54.052c5.521,5.521,12.854,8.563,20.646,8.563h68.125 c7.792,0,15.125-3.042,20.646-8.563l54.063-54.052c5.521-5.521,8.563-12.865,8.563-20.667v-68.104 C512,288.813,508.958,281.469,503.438,275.948z M42.667,85.333h320c0.443,0,0.814,0.225,1.25,0.253L211.688,210.604 c-5.583,3.625-13.417,2.938-17.083,0.688L41.424,85.585C41.858,85.559,42.227,85.333,42.667,85.333z M277.333,296.615v44.719 H42.667c-11.771,0-21.333-9.573-21.333-21.333V106.667c0-3.021,0.668-5.875,1.805-8.482l158.883,130.294 c6.208,4.052,13.354,6.188,20.646,6.188c7.25,0,14.396-2.125,21.646-6.885L382.194,98.184c1.138,2.608,1.806,5.461,1.806,8.483 v106.667h-23.396c-7.792,0-15.125,3.042-20.646,8.563l-54.063,54.052C280.375,281.469,277.333,288.813,277.333,296.615z M490.667,364.719c0,2.104-0.813,4.094-2.313,5.583l-54.063,54.052c-1.5,1.49-3.458,2.313-5.563,2.313h-68.125 c-2.104,0-4.063-0.823-5.563-2.313l-54.063-54.052c-1.5-1.49-2.313-3.479-2.313-5.583v-68.104c0-2.104,0.813-4.094,2.313-5.583 l54.063-54.052c1.5-1.49,3.458-2.313,5.563-2.313h68.125c2.104,0,4.063,0.823,5.563,2.313l54.063,54.052 c1.5,1.49,2.313,3.479,2.313,5.583V364.719z'
                                                        />
                                                        <path
                                                            d='M394.667,256c-5.896,0-10.667,4.771-10.667,10.667V352c0,5.896,4.771,10.667,10.667,10.667s10.667-4.771,10.667-10.667 v-85.333C405.333,260.771,400.563,256,394.667,256z'
                                                        />
                                                        <circle cx='394.667' cy='394.667' r='10.667'/>
                                                    </svg>
                                                </a>
                                            </div>
                                        </div>
                                        <div className="col">

                                            <div className="content_page_links">
                                                <a href="#">
                                                    <svg data-toggle='tooltip' id='Layer_1'
                                                         xmlns='http://www.w3.org/2000/svg'
                                                         viewBox='0 0 512 512'>
                                                        <path
                                                            d='M405.333,213.874V106.667c0-23.531-19.146-42.667-42.667-42.667h-320C19.146,64,0,83.135,0,106.667V320 c0,23.531,19.146,42.667,42.667,42.667h239.216C295.862,411.84,341.082,448,394.667,448C459.354,448,512,395.365,512,330.667 C512,269.57,465.036,219.289,405.333,213.874z M42.667,85.333h320c0.443,0,0.814,0.225,1.25,0.253L211.688,210.604 c-5.583,3.625-13.417,2.938-17.083,0.688L41.424,85.585C41.858,85.559,42.227,85.333,42.667,85.333z M277.874,341.333H42.667 c-11.771,0-21.333-9.573-21.333-21.333V106.667c0-3.021,0.668-5.875,1.805-8.482l158.883,130.294 c6.208,4.052,13.354,6.188,20.646,6.188c7.25,0,14.396-2.125,21.646-6.885L382.194,98.184c1.138,2.608,1.806,5.461,1.806,8.483 v107.207c-59.703,5.415-106.667,55.697-106.667,116.793C277.333,334.267,277.555,337.815,277.874,341.333z M394.667,426.667 c-52.938,0-96-43.063-96-96c0-22.624,8.193-43.167,21.332-59.594l134.251,134.27C437.826,418.478,417.286,426.667,394.667,426.667 z M469.335,390.26l-134.251-134.27c16.424-13.135,36.964-21.324,59.583-21.324c52.938,0,96,43.063,96,96 C490.667,353.29,482.474,373.833,469.335,390.26z'
                                                        />
                                                    </svg>
                                                </a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="col">
                                        <div className="content_page_tools">
                                            <a href="javascript:;" className="btn icon-btn" id="table-state">
														<span>
															Bütün jurnal
														</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div className="content_table" style={{display: ''}}>
                            <div className="row ">

                                <div className="col" id="short-table">
                                    <div className="row">
                                        <div className="col-md-7 ">
                                            <table className="inout-table ">
                                                <thead>
                                                <tr>
                                                    <th sort="asc">
                                                        <div className="checkbox checkbox-success">
                                                            <input className="check ckeck-head" id="checkbox123"
                                                                   type="checkbox"/>
                                                            <label htmlFor="checkbox123"></label>
                                                        </div>
                                                    </th>
                                                    <th sort="desc">
                                                        <div>№</div>
                                                    </th>
                                                    <th sort="desc">
                                                        <div>Email</div>
                                                    </th>
                                                    <th sort="asc">
                                                        <div> Paket</div>
                                                    </th>
                                                    <th sort="asc">
                                                        <div> Şirkət</div>
                                                    </th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <div className="checkbox checkbox-success">
                                                            <input className="check" id="checkbox1" type="checkbox"/>
                                                            <label htmlFor="checkbox1"></label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <span>1 </span>
                                                    </td>
                                                    <td>
                                                        <span> Məmmədli@gmail.com </span>
                                                    </td>
                                                    <td>
                                                        <div>
                                                            <span>Office</span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div>
                                                            <span>Pronet</span>
                                                        </div>
                                                    </td>
                                                </tr>

                                                </tbody>


                                            </table>
                                        </div>
                                        <div className="col-md-5">
                                            <div className="wrapper">
                                                <div className="tabs head">
                                                    <div className="col-md-4 tab">
                                                        <span className="">Ətraflı</span>

                                                    </div>
                                                    <div className="col-md-4 tab">
                                                        <span className="">Tarİxçə</span>

                                                    </div>
                                                    <div className="col-md-4 tab">
                                                        <span className="">Ödənişlər</span>

                                                    </div>
                                                </div>
                                                <div className="tab_content ">
                                                    <div className="tab_item">
                                                        <ul>
                                                            <li>
                                                                <p className="p-gray">Email</p>
                                                                <p>Memmeli@gmail.com</p>
                                                            </li>
                                                            <li>
                                                                <p className="p-gray">Paket</p>
                                                                <p>Office +</p>
                                                            </li>
                                                            <li>
                                                                <p className="p-gray">Şirkət</p>
                                                                <p>Pronet</p>
                                                            </li>
                                                            <li>
                                                                <p className="p-gray">Qoşulma Müddəti</p>
                                                                <p>14</p>
                                                            </li>
                                                            <li>
                                                                <p className="p-gray">Son Aktivlik</p>
                                                                <p>12.08.2018</p>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div className="tab_item">
                                                        <ul>
                                                            <li>
                                                                <p className="p-gray">Email</p>
                                                                <p>Memmeli@gmail.com</p>
                                                            </li>
                                                            <li>
                                                                <p className="p-gray">Paket</p>
                                                                <p>Office +</p>
                                                            </li>
                                                            <li>
                                                                <p className="p-gray">Şirkət</p>
                                                                <p>Pronet</p>
                                                            </li>
                                                            <li>
                                                                <p className="p-gray">Qoşulma Müddəti</p>
                                                                <p>14</p>
                                                            </li>
                                                            <li>
                                                                <p className="p-gray">Son Aktivlik</p>
                                                                <p>12.08.2018</p>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div className="tab_item">
                                                        <ul>
                                                            <li>
                                                                <p className="p-gray">Email</p>
                                                                <p>Memmeli@gmail.com</p>
                                                            </li>
                                                            <li>
                                                                <p className="p-gray">Paket</p>
                                                                <p>Office +</p>
                                                            </li>
                                                            <li>
                                                                <p className="p-gray">Şirkət</p>
                                                                <p>Pronet</p>
                                                            </li>
                                                            <li>
                                                                <p className="p-gray">Qoşulma Müddəti</p>
                                                                <p>14</p>
                                                            </li>
                                                            <li>
                                                                <p className="p-gray">Son Aktivlik</p>
                                                                <p>12.08.2018</p>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>


                                <div className="col " id="full-table" style={{display: 'none'}}>
                                    <table className="inout-table ">
                                        <thead>
                                        <tr>
                                            <th sort="asc">
                                                <div className="checkbox checkbox-success">
                                                    <input className="check ckeck-head" id="checkbox1234"
                                                           type="checkbox"/>
                                                    <label htmlFor="checkbox1234"></label>
                                                </div>
                                            </th>
                                            <th sort="desc">
                                                <div>№</div>
                                            </th>
                                            <th sort="desc">
                                                <div>Email</div>
                                            </th>
                                            <th sort="asc">
                                                <div> Paket</div>
                                            </th>
                                            <th sort="asc">
                                                <div> Şirkət</div>
                                            </th>
                                            <th sort="asc">
                                                <div>Qoşulma muddəti</div>
                                            </th>
                                            <th sort="asc">
                                                <div>Son aktivlik</div>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <div className="checkbox checkbox-success">
                                                    <input className="check" id="checkbox10" type="checkbox"/>
                                                    <label htmlFor="checkbox10"></label>
                                                </div>
                                            </td>
                                            <td>
                                                <span>1</span>
                                            </td>
                                            <td>
                                                <span> Məmmədli@gmail.com </span>
                                            </td>
                                            <td>
                                                <div>
                                                    <span>Office</span>
                                                </div>
                                            </td>
                                            <td>
                                                <div>
                                                    <span>Pronet</span>
                                                </div>
                                            </td>
                                            <td>
                                                <div>
                                                    <span>12</span>
                                                </div>
                                            </td>
                                            <td>
                                                <div>
                                                    <span>24.07.2018</span>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default Account;