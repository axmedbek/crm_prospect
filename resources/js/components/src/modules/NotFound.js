import React from 'react';

class NotFound extends React.Component{
    render(){
        return(
            <section className="content">
                <div className="container-fluid">
                    <p>NOT FOUND 404</p>
                </div>
            </section>
        );
    }
}

export default NotFound;