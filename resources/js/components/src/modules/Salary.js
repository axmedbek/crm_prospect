import React from 'react';
import SalarySideSection from '../sidesections/SalarySideSection';

class Salary extends React.Component {
    render() {
        return (
            <div>
                <SalarySideSection/>
                <section className="content">
                    <div className="container-fluid">
                        <div className="content_date">
                            <p className="date_from_to">21 Sentyabr -28 Sentyabr</p>
                        </div>
                        <div className="content_header_btn_group">
                            <div className="row">
                                <div className="col-md-9">
                                    <div className="row">
                                        <div className="col-md-4">
                                            <div className="tile_btn ">
                                                <a href="payments.html" className="active">
                                                    <div className="btn_left_side green ">
                                                        <div className="text">
                                                            <p className="pay">Ödənilib</p>

                                                        </div>
                                                    </div>
                                                    <div className="btn_right_side">
                                                        <div className="text_center">
                                                            <p>100Azn</p>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <div className="col-md-4">
                                            <div className="tile_btn ">
                                                <a href="time-over.html">
                                                    <div className="btn_left_side blue">
                                                        <div className="text">
                                                            <p className="pay">Vaxtı Çatıb</p>

                                                        </div>
                                                    </div>
                                                    <div className="btn_right_side">
                                                        <div className="text_center">
                                                            <p>100Azn</p>
                                                            <span className="red span-down"></span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <div className="col-md-4">
                                            <div className="tile_btn ">
                                                <a href="time-pass.html">
                                                    <div className="btn_left_side yellow">
                                                        <div className="text">
                                                            <p className="pay">Vaxtı kecib</p>

                                                        </div>
                                                    </div>
                                                    <div className="btn_right_side">
                                                        <div className="text_center">
                                                            <p>100Azn</p>
                                                            <span className="green span-up"></span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div className="col-md-3"></div>
                            </div>
                        </div>
                        <div className="content_table" style={{display: ''}}>
                            <div className="row ">

                                <div className="col" id="short-table">
                                    <div className="row">
                                        <div className="col-md-7 ">
                                            <table className="inout-table ">
                                                <thead>
                                                <tr>
                                                    <th sort="asc">
                                                        <div className="checkbox checkbox-success">
                                                            <input className="check ckeck-head" id="checkbox123"
                                                                   type="checkbox"/>
                                                            <label htmlFor="checkbox123"></label>
                                                        </div>
                                                    </th>
                                                    <th sort="desc">
                                                        <div>№</div>
                                                    </th>
                                                    <th sort="desc">
                                                        <div>Email</div>
                                                    </th>

                                                    <th sort="asc">
                                                        <div> Şirkət</div>
                                                    </th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <div className="checkbox checkbox-success">
                                                            <input className="check" id="checkbox1" type="checkbox"/>
                                                            <label htmlFor="checkbox1"></label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <span>1</span>
                                                    </td>
                                                    <td>
                                                        <span> Məmmədli@gmail.com </span>
                                                    </td>
                                                    <td>
                                                        <div>
                                                            <span>Pronet</span>
                                                        </div>
                                                    </td>
                                                </tr>

                                                </tbody>


                                            </table>
                                        </div>
                                        <div className="col-md-5">
                                            <div className="wrapper">
                                                <div className="tabs head">
                                                    <div className="col-md-4 tab">
                                                        <span className="">Ətraflı</span>

                                                    </div>
                                                    <div className="col-md-4 tab">
                                                        <span className="">Tarİxçə</span>

                                                    </div>
                                                    <div className="col-md-4 tab">
                                                        <span className="">Ödənişlər</span>

                                                    </div>
                                                </div>
                                                <div className="tab_content ">
                                                    <div className="tab_item">
                                                        <ul>
                                                            <li>
                                                                <p className="p-gray">Email</p>
                                                                <p>Memmeli@gmail.com</p>
                                                            </li>
                                                            <li>
                                                                <p className="p-gray">Paket</p>
                                                                <p>Office +</p>
                                                            </li>
                                                            <li>
                                                                <p className="p-gray">Şirkət</p>
                                                                <p>Pronet</p>
                                                            </li>
                                                            <li>
                                                                <p className="p-gray">Qoşulma Müddəti</p>
                                                                <p>14</p>
                                                            </li>
                                                            <li>
                                                                <p className="p-gray">Son Aktivlik</p>
                                                                <p>12.08.2018</p>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div className="tab_item">
                                                        <ul>
                                                            <li>
                                                                <p className="p-gray">Email</p>
                                                                <p>Memmeli@gmail.com</p>
                                                            </li>
                                                            <li>
                                                                <p className="p-gray">Paket</p>
                                                                <p>Office +</p>
                                                            </li>
                                                            <li>
                                                                <p className="p-gray">Şirkət</p>
                                                                <p>Pronet</p>
                                                            </li>
                                                            <li>
                                                                <p className="p-gray">Qoşulma Müddəti</p>
                                                                <p>14</p>
                                                            </li>
                                                            <li>
                                                                <p className="p-gray">Son Aktivlik</p>
                                                                <p>12.08.2018</p>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div className="tab_item">
                                                        <ul>
                                                            <li>
                                                                <p className="p-gray">Email</p>
                                                                <p>Memmeli@gmail.com</p>
                                                            </li>
                                                            <li>
                                                                <p className="p-gray">Paket</p>
                                                                <p>Office +</p>
                                                            </li>
                                                            <li>
                                                                <p className="p-gray">Şirkət</p>
                                                                <p>Pronet</p>
                                                            </li>
                                                            <li>
                                                                <p className="p-gray">Qoşulma Müddəti</p>
                                                                <p>14</p>
                                                            </li>
                                                            <li>
                                                                <p className="p-gray">Son Aktivlik</p>
                                                                <p>12.08.2018</p>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col " id="full-table" style={{display: 'none'}}>
                                    <table className="inout-table ">
                                        <thead>
                                        <tr>
                                            <th sort="asc">
                                                <div className="checkbox checkbox-success">
                                                    <input className="check ckeck-head" id="checkbox123"
                                                           type="checkbox"/>
                                                    <label htmlFor="checkbox123"></label>
                                                </div>
                                            </th>
                                            <th sort="desc">
                                                <div>№</div>
                                            </th>
                                            <th sort="desc">
                                                <div>Email</div>
                                            </th>

                                            <th sort="asc">
                                                <div> Şirkət</div>
                                            </th>
                                            <th sort="asc">
                                                <div>Cari Paket</div>
                                            </th>
                                            <th sort="asc">
                                                <div>Ödənilib</div>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <div className="checkbox checkbox-success">
                                                    <input className="check" id="checkbox1" type="checkbox"/>
                                                    <label htmlFor="checkbox1"></label>
                                                </div>
                                            </td>
                                            <td>
                                                <span>1</span>
                                            </td>
                                            <td>
                                                <span> Məmmədli@gmail.com </span>
                                            </td>

                                            <td>
                                                <div>
                                                    <span>Pronet</span>
                                                </div>
                                            </td>
                                            <td>
                                                <div>
                                                    <span>Office</span>
                                                </div>
                                            </td>
                                            <td>
                                                <div>
                                                    <span>25Azn</span>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default Salary;