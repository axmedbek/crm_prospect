import React from 'react';
import TransactionSideSection from '../sidesections/TransactionSideSection';

class Transaction extends React.Component {
    render() {
        return (
            <div>
                <TransactionSideSection/>
                <section className="content">
                    <div className="container-fluid">
                        <div className="content_date">
                            <p className="date_from_to">21 Sentyabr -28 Sentyabr</p>
                        </div>
                        <div className="content_header_btn_group">
                            <div className="row">
                                <div className="col">
                                    <div className="tile_btn ">
                                        <a href="transition.html" className="active">
                                            <div className="btn_left_side green ">
                                                <div className="text">
                                                    <p>Free</p>
                                                    <p>100</p>
                                                </div>
                                            </div>
                                            <div className="btn_right_side">
                                                <div className="text">
                                                    <p>100%</p>
                                                    <span className="green span-up"></span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="tile_btn ">
                                        <a href="tra-office.html">
                                            <div className="btn_left_side blue">
                                                <div className="text">
                                                    <p>Office</p>
                                                    <p>100</p>
                                                </div>
                                            </div>
                                            <div className="btn_right_side">
                                                <div className="text">
                                                    <p>100%</p>
                                                    <span className="red span-down"></span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="tile_btn ">
                                        <a href="tra-office-plus.html">
                                            <div className="btn_left_side yellow">
                                                <div className="text">
                                                    <p>Office+</p>
                                                    <p>100</p>
                                                </div>
                                            </div>
                                            <div className="btn_right_side">
                                                <div className="text">
                                                    <p>100%</p>
                                                    <span className="green span-up"></span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="tile_btn ">
                                        <a href="tra-company.html">
                                            <div className="btn_left_side red">
                                                <div className="text">
                                                    <p>Company</p>
                                                    <p>100</p>
                                                </div>
                                            </div>
                                            <div className="btn_right_side">
                                                <div className="text">
                                                    <p>100%</p>
                                                    <span className="red span-down"></span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div className="col">
                                    <div className="tile_btn  ">
                                        <a href="tra-cancel.html">
                                            <div className="btn_left_side orange">
                                                <div className="text">
                                                    <p>Imtina</p>
                                                    <p>100</p>
                                                </div>
                                            </div>
                                            <div className="btn_right_side">
                                                <div className="text">
                                                    <p>100%</p>
                                                    <span className="green span-up"></span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="content_table" style={{display: ''}}>
                            <div className="row ">
                                <div className="col " id="short-table">
                                    <table className="inout-table ">
                                        <thead>
                                        <tr>
                                            <th sort="asc">
                                                <div className="checkbox checkbox-success">
                                                    <input className="check ckeck-head" id="checkbox123"
                                                           type="checkbox"/>
                                                    <label htmlFor="checkbox123"></label>
                                                </div>
                                            </th>
                                            <th sort="desc">
                                                <div>№</div>
                                            </th>
                                            <th sort="desc">
                                                <div>Email</div>
                                            </th>
                                            <th sort="asc">
                                                <div> Şirkət</div>
                                            </th>
                                            <th sort="asc">
                                                <div>Hardan Keçib</div>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <div className="checkbox checkbox-success">
                                                    <input className="check" id="checkbox1" type="checkbox"/>
                                                    <label htmlFor="checkbox1"></label>
                                                </div>
                                            </td>
                                            <td>
                                                <span>1</span>
                                            </td>
                                            <td>
                                                <span> Məmmədli@gmail.com </span>
                                            </td>
                                            <td>
                                                <div>
                                                    <span>Pronet</span>
                                                </div>
                                            </td>
                                            <td>
                                                <div>
                                                    <span>office+</span>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default Transaction;