import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import Header from './src/Header';
import Sidebar from './src/Sidebar';
import {Route , Switch } from 'react-router-dom';
import Account from './src/modules/Account';
import Salary from './src/modules/Salary';
import Transaction from './src/modules/Transaction';
import NotFound from './src/modules/NotFound';
import {BrowserRouter} from 'react-router-dom';


export default class App extends Component {

    componentWillMount() {
        // loadjs('../../../public/js/scripts.min.js', function() {
        // });
    }
    render() {
        return (
            <div>
                <Header/>
                <Sidebar/>
                <Switch>
                    <Route exact path='/' component={Account}/>
                    <Route exact path='/salary' component={Salary}/>
                    <Route exact path='/attendance' component={Transaction}/>
                    <Route exact component={NotFound}/>
                </Switch>
            </div>
        );
    }
}

if (document.getElementById('root')) {
    ReactDOM.render(
        <BrowserRouter>
            <App/>
        </BrowserRouter>, document.getElementById('root'));
}
